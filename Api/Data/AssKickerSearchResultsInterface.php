<?php


namespace MageTemp\TestTest\Api\Data;

interface AssKickerSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get AssKicker list.
     * @return \MageTemp\TestTest\Api\Data\AssKickerInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \MageTemp\TestTest\Api\Data\AssKickerInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
