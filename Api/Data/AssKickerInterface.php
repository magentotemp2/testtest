<?php


namespace MageTemp\TestTest\Api\Data;

interface AssKickerInterface
{

    const ASSKICKER_ID = 'asskicker_id';
    const ID = 'id';

    /**
     * Get asskicker_id
     * @return string|null
     */
    public function getAsskickerId();

    /**
     * Set asskicker_id
     * @param string $asskickerId
     * @return \MageTemp\TestTest\Api\Data\AssKickerInterface
     */
    public function setAsskickerId($asskickerId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \MageTemp\TestTest\Api\Data\AssKickerInterface
     */
    public function setId($id);
}
