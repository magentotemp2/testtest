<?php


namespace MageTemp\TestTest\Model\ResourceModel\AssKicker;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \MageTemp\TestTest\Model\AssKicker::class,
            \MageTemp\TestTest\Model\ResourceModel\AssKicker::class
        );
    }
}
