<?php


namespace MageTemp\TestTest\Model\ResourceModel;

class AssKicker extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magetemp_testtest_asskicker', 'asskicker_id');
    }
}
