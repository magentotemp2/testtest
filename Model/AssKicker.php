<?php


namespace MageTemp\TestTest\Model;

use MageTemp\TestTest\Api\Data\AssKickerInterface;

class AssKicker extends \Magento\Framework\Model\AbstractModel implements AssKickerInterface
{

    protected $_eventPrefix = 'magetemp_testtest_asskicker';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\MageTemp\TestTest\Model\ResourceModel\AssKicker::class);
    }

    /**
     * Get asskicker_id
     * @return string
     */
    public function getAsskickerId()
    {
        return $this->getData(self::ASSKICKER_ID);
    }

    /**
     * Set asskicker_id
     * @param string $asskickerId
     * @return \MageTemp\TestTest\Api\Data\AssKickerInterface
     */
    public function setAsskickerId($asskickerId)
    {
        return $this->setData(self::ASSKICKER_ID, $asskickerId);
    }

    /**
     * Get id
     * @return string
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set id
     * @param string $id
     * @return \MageTemp\TestTest\Api\Data\AssKickerInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }
}
